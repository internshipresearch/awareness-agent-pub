# Agent Module #

Agent Module is a part of "**Awareness: Alerting for Developers**", it will be deployed on the client machine in order to test resources like Services, Network, API, File Change and Mysql Connection for current status so that alerts can be generated at the application level.

### What is this repository for? ###
* To check application current status running on the client machine and send them to the centralized alerting system.
* Version: 1.0
* For better understanding of each momdule please read this [Quick Documentation](https://goo.gl/LEz2jQ).

### Prerequisites ###
Python 2.7

### Installing ###
* Clone the repo in **/var** directory
```
#!GIT
cd /var
git clone https://ashysaini@bitbucket.org/internshipresearch/awareness-agent-pub.git

```
* Navigate to the working directiry **/var/awareness-agent-pub**
```
#!GIT
cd awareness-agent-pub

```
* In order to use agent_module first you need to register it with our central server using **register.py** file 
present in the project working directory. 
Run this file uisng following command, it will ask you for some config details like server name, server Ip address and
also login credentials which you will be provided througn an email so fill the input and complete registration.
```
#!GIT
sudo python register.py
```
* Once the registration process is done you can navigate to  **modules_config** inside **/var/awareness-agent-pub/config** directory
```
#!GIT
cd config/modules_config

```
* Now create configuration files for the applications running on your server, these application config files must be created inside **/var/awareness-agent-pub/config/modules_config** directory of your project, you can give them any name but the extension should but (**.json**).

#### API Module ####
* To check any api for its status code and response create a file with any name and paste the following code and change values accordingly
```
#!JSON
{
    "api" : [
          {
            "url" : "http://your-domain.com/rule-api/public/action",
            "request_method" : "POST",
            "code" : 200,
            "response" : "msg",
            "error_msg": "api down"
          }
        ]
}

```

#### FileChange Module ####
* To check a filechange in a given time create another file and paste following code
```
#!JSON
{
    "filechanged" : [
          {
            "name" : "/tmp/ga*",
            "seconds" : 60,
            "status" : 1,
            "error_msg": "file not changed"
          }
        ]
}
```
#### Mysql Module ####
* To check **mysql connection** make another file and paste the following code
```
#!JSON
{
    "mysql" : [
          {
            "name" : "db_connection",
            "hostname" : "localhost",
            "username" : "root",
            "password" : "gaurav",
            "error_msg": "mysql connectin error"
          }
        ]
}
```
#### Network Module ####
* To check network make another file and paste the following code
```
#!JSON
{
    "network" : [
          {
          "name" : "facebook.com",
          "status" : 1,
          "error_msg": "network unreachable"
          }
        ]
}
```
#### Service Module ####
* To check any service make another file and paste the following code.
**Note**: service_path is optional
```
#!JSON
{
    "service" : [
          {
            "name": "cron,crond",
            "service_path":"/usr/sbin/service",
            "status": 1,
            "error_msg": "cron down"
          }
        ]
}
```
#### Command Module ####
* To check any command make another file and paste the following code.
* Here you can give any command, and check its output as success_text for health and error_text for the alert. **nz_exit_code** can be set **1** if you want to check error_text even in case of non-zero exit code, and **0** if you don't want to strictly check it.
```
#!JSON
{
    "cmd" : [
          {
            "cmd": " service apache2 status ",
            "nz_exit_code":"1",
            "success_text": "running",
            "error_text": "dead",
            "error_msg": "command error",
            "name": "apache_cmd"
          }
        ]
}
```
#### Memcache Module ####
* To check memcache make another file and paste the following code.
```
#!JSON
{
    "memcache" : [
          {
            "host":"localhost",
            "port":"11211",
            "mem_cache_key" :"companies",
            "error_msg": "memcache error"
          }
        ]
}
```

* You can create any number of these application config files with different names but must be placed in **config/modules_config** directory.
* If you want to check multiple same types of applications, Let's say service like Apache and Asterisk then create a new file for each service and paste above json code for service and change the name with your service name.
* Now run the main.py file and check if it is working fine.
```
#!GIT
cd ../..
sudo python main.py

```
* Create **Cron** or **Supervisor** on main.py file of this cloned repo.
#### Setting a cron ####
* Open the terminal and paste the following command
```
#!GIT
crontab -e

```
* A cron file will be open then append the following command and save it.
```
#!GIT
* * * * * /usr/bin/python  /var/awareness-agent-pub/main.py >> /tmp/agent.log

```
#### Setting a supervisor ####
* Open the terminal and paste following command
```
#!GIT
sudo vim /etc/supervisor/supervisord.conf

```
* supervisor.conf file will be open then append following command
```
#!GIT
[program:agent]
command=/usr/bin/python  /var/awareness-agent-pub/main.py  >> /tmp/agent.log
startsecs=0
autorestart=true
 

```