__author__ = 'ashysaini'
__project__ = 'awareness'

'''
    Main file of the project
    usage : python main.py
    helper files :
        config/config.json : contains all configurations for all tools/modules
        utils.py : contains the definations used by main file

    Extra pythonpackage required : click Now Replaced by Argparse
'''


import utils
import multiprocessing
import time

parent_config = utils.load_config('config.json',1)
config_files = utils.load_config_files_path()

for key in config_files:
    module = utils.load_config(key,0)
    if 'service' in module:
        for service in module['service']:
            service_name = service['name']
            try:
                service_path = service['service_path']
            except Exception as e:
                service_path = ''
            
            try:
                error_msg = service['error_msg']
            except Exception as e:
                error_msg = ''

            p = multiprocessing.Process(target=utils.service_worker, args=(service_name,service_path, error_msg))
            p.start()

    if 'network' in module:
        for network in module['network']:
            print network['name']
            try:
                error_msg = network['error_msg']
            except Exception as e:
                error_msg = ''
            p = multiprocessing.Process(target=utils.network_worker, args=(network['name'],error_msg))
            p.start()


    if 'filechanged' in module:
        for file in module['filechanged']:
            print file['name']
            try:
                error_msg = file['error_msg']
            except Exception as e:
                error_msg = ''

            p = multiprocessing.Process(target=utils.filechanged_worker, args=(file['name'],file['seconds'],file['status'], error_msg))
            p.start()

    if 'mysql' in module:
        for connection in module['mysql']:
            print connection['name']
            try:
                error_msg = connection['error_msg']
            except Exception as e:
                error_msg = ''

            p = multiprocessing.Process(target=utils.mysql_worker, args=(connection['name'] , connection['hostname'], connection['username'], connection['password'], error_msg))
            p.start()

    if 'api' in module:
        for api in module['api']:
            try:
                error_msg = api['error_msg']
            except Exception as e:
                error_msg = ''

            p = multiprocessing.Process(target=utils.api_worker, args=(api['url'],api['request_method'], api['code'], api['response'], error_msg))
            p.start()


    if 'cmd' in module:
        for cmd in module['cmd']:
            try:
                success_text = cmd['success_text']
            except Exception as e:
                success_text = '88888888abcd'
                
            try:
                error_text = cmd['error_text']
            except Exception as e:
                error_text = '88888888abcd'

            try:
                nz_exit_code = cmd['nz_exit_code']
            except Exception as e:
                nz_exit_code = '0'

            try:
                error_msg = cmd['error_msg']
            except Exception as e:
                error_msg = ''

            try:
                name = cmd['name']
            except Exception as e:
                name = ''


            p = multiprocessing.Process(target=utils.cmd_worker, args=(cmd['cmd'],nz_exit_code, success_text, error_text, error_msg, name))
            p.start()

    if 'memcache' in module:
        for memcache in module['memcache']:
            print memcache['host'],memcache['port']
            try:
                error_msg = memcache['error_msg']
            except Exception as e:
                error_msg = ''

            if 'mem_cache_key' in memcache:
                p = multiprocessing.Process(target=utils.memcache_worker, args=(memcache['host'],memcache['port'],error_msg, memcache['mem_cache_key']))
            else:
                p = multiprocessing.Process(target=utils.memcache_worker, args=(memcache['host'],memcache['port'], error_msg))
            p.start()
    


    p = multiprocessing.Process(target=utils.api_action)
    p.start()
    time.sleep(parent_config['rest_interval'])


