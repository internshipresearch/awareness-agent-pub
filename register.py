'''
    This file is for registration the centralized server.
    In order to use this file you need run this file,
    It will ask you some details like email, password, server name, ip etc.
    please provide input to complete registration.
    usage : python register.py
'''


import io, json, os, re, urllib, urllib2

def read_input():
    config = {}
    print ("Please enter the following information to complete registration with central server")
    config['ip'] = raw_input('Enter server public IP: ')
    config['server_id'] = raw_input('Enter server name: ')
    config['email'] = raw_input('Enter user email: ')
    config['password'] = raw_input('Enter user password: ')
    config['register_url'] = raw_input('Enter URL: ')
    return config

def validate_data(config):
    req_params = ['email', 'password', 'ip', 'register_url', 'server_id']
    missing = []
    for key in req_params:
        if config[key]:
            pass
        else:
            missing.append(key)
    if missing:
        print("Input can't be empty", missing)



def write_json(rsp, file):
    # Make it work for Python 2+3 and with Unicode
    try:
        #assure_path_exists(file)
        to_unicode = unicode
    except NameError:
        to_unicode = str
    with io.open(file, 'w', encoding='utf8') as outfile:
        str_ = json.dumps(rsp, indent=4, sort_keys=True, separators=(',', ': '), ensure_ascii=False)
        outfile.write(to_unicode(str_))


def assure_path_exists(path):
    dir = os.path.dirname(path)
    if not os.path.exists(dir):
        os.makedirs(dir)


def register(config):
    url = config['register_url']
    data = {"email": config['email'], "password": config['password'], "ip": config['ip'], "server_id": config['server_id']}
    data = urllib.urlencode(data)
    req = urllib2.Request(url, data)

    try:
        response = urllib2.urlopen(req)
    except urllib2.HTTPError, e:
        print e.read()
        exit()
    except Exception as e:
        print ('Problem in connection with API, Error: %s' %e)
        exit()
    rsp = response.read()
    response.close()
    return json.loads(rsp)['data']

if __name__ == '__main__':
    abs_path = os.path.dirname(os.path.abspath(__file__))
    config_file = abs_path + '/config/config.json'
    modules_config_path = abs_path + '/config/modules_config/'
    input_data = read_input()
    validate_data(input_data)
    config_data = register(input_data)
    assure_path_exists(modules_config_path)
    write_json(config_data, config_file)
    print "Registration Successful, Please check config.json file in config folder of project directory"