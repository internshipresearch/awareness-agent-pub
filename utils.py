__author__ = 'ashysaini'

import json
import os
import urllib
import urllib2

abs_path = os.path.dirname(os.path.abspath(__file__))
def load_config_files_path():
    path = os.path.dirname(os.path.abspath(__file__))+'/config/modules_config'
    all_files = []
    files = []
    for path, subdirs, files in os.walk(path):
       'pass'
       #all_files.extend(files)
    return files

def load_config(filename , parent):
    if parent==0:
        try:
            #loads the config data from json file
            with open(abs_path + '/config/modules_config/'+filename, 'r') as config_file:
                config_data = config_file.read()
                config_file.close()
            return json.loads(config_data)
        except Exception as e:
            print ('Invalid json data in %s: %s' %(filename, e))
            exit()
    if parent==1:
        try:
            with open(abs_path + '/config/'+filename, 'r') as config_file:
                config_data = config_file.read()
                config_file.close()
            return json.loads(config_data)
        except Exception as e:
            print ('Invalid json data in %s: %s' %(filename, e))
            exit()


def service_worker(service_name, service_path, error_msg):
    os.system("timeout 10s python "+ abs_path +'/modules/service.py --service="%s" --service_path="%s"  --error_msg="%s"' %(service_name, service_path, error_msg))

def network_worker(url, error_msg):
    os.system("timeout 10s python " + abs_path + '/modules/network.py --url="%s" --error_msg="%s"' %(url, error_msg))

def filechanged_worker(file, seconds, status,error_msg):
    os.system("timeout 10s python " + abs_path + '/modules/filechanged.py --file="%s" --seconds=%d --status=%d --error_msg="%s"' %(file, seconds, status, error_msg))

def mysql_worker(name, hostname, username, password, error_msg):
    os.system("timeout 10s python " + abs_path + '/modules/mysql.py --name="%s" --hostname="%s" --username="%s" --password="%s" --error_msg="%s"' %(name, hostname, username, password, error_msg))

def api_worker(url, method, code, response, error_msg):
    os.system("timeout 10s python " + abs_path + '/modules/api.py --url="%s" --method="%s" --code="%s" --response="%s" --error_msg="%s"' %(url, method, code, response, error_msg))


def cmd_worker(cmd, nz_exit_code, success_text, error_text, error_msg, name):
    os.system("timeout 10s python " + abs_path + '/modules/cmd.py --cmd="%s" --nz_exit_code="%s" --success_text="%s" --error_text="%s" --error_msg="%s" --name="%s"' %(cmd, nz_exit_code, success_text, error_text, error_msg, name))


def memcache_worker(host, port, error_msg, key = None):
    if key is None:
        os.system("timeout 10s python " + abs_path + '/modules/memcache.py --host="%s" --port="%s" --error_msg="%s"' %(host, port, error_msg))
    else:
        os.system("timeout 10s python " + abs_path + '/modules/memcache.py --host="%s" --port="%s" --error_msg="%s" --key="%s"' %(host, port,error_msg, key))


def api_action():
    if os.path.exists(abs_path + "/logs/api.dat"):
        num_lines = sum(1 for line in open(abs_path + "/logs/api.dat"))
        if num_lines>4:
            os.rename(abs_path + "/logs/api.dat",abs_path + "/logs/executable.dat")
            with open(abs_path + "/logs/executable.dat") as myfile:
                config_data = load_config('config.json',1)
                es_url = config_data['es_url']
                auth = config_data['authorization']
                for line in myfile:
                    '''"uncomment the below code"'''

                    headers = {"Authorization": auth}
                    data = json.loads(line)
                    data = urllib.urlencode(data)
                    req = urllib2.Request(es_url, data, headers)
                    try:
                        response = urllib2.urlopen(req)
                    except Exception as e:
                        print "Unable to connect"
                        print e.reason
                        exit()
                    the_page = response.read()
                    print response.getcode()
                    print the_page
                    response.close()
                os.remove(abs_path + "/logs/executable.dat")
