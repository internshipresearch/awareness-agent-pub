__author__ = 'ashysaini'

'''
    CLI tool for checking the status of a service
    this module will check the service based on the parameters provided
    if the service is not running then an alert will be generated (can be used vise-versa)
    Arguments :
        --service : name of the service to perform action on
        --action (optional) : action to be performed by CLI
        --help : show help section
'''

import mod_utils
import subprocess
import argparse
import time

action_type = "service"

parser = argparse.ArgumentParser()
parser.add_argument('--service', default='apache2', help='Name of the service, like apache2 mysql etc.')
parser.add_argument('--action', default='status', help='Action for service, like status, restart, stop, start etc.')
parser.add_argument('--service_path', default='/sbin/service', help='Path of service in your os eg. /usr/sbin/service')
parser.add_argument("--error_msg", help="Error msg which you want to send in alerts", default='')

args = parser.parse_args()

def begin(action, service, service_path, error_msg):
    global  action_type

    # services =  [x.strip() for x in service.split(',')]
    services =  service.split(",")

    if service_path =='':
        #find the path of service
        cmd = "which service; echo $?"
        proc =  subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
        # service_cmd = proc.stdout.read().strip()

        r, err = proc.communicate()
        r = r.split('\n')
        print r
        if r[-2] == '0' and 'service' in r[0]:
            service_cmd = r[0]
        else:
            service_cmd = '/sbin/service'
    else:
        service_cmd = service_path


    #if there are one service name or many
    #in case of many check which one is correct
    if len(services) > 1:
        print "in len"
        service_exist = ""
        #check which service name exist
        for service_name in services:

            cmd = "timeout 5s %s %s %s" %(service_cmd, service_name, action)
            print "running command %s" %cmd
            proc =  subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
            cmd_out = proc.stdout.read()
            if "not-found" in cmd_out or "unrecognized" in cmd_out:
                print "not found or unrecognised"
                continue
            else:
                service_exist = service_name
        if service_exist == "":
            print "no service found with this name"
    else:
        service_exist = services[0]

    cmd = "timeout 5s %s %s %s" %(service_cmd, service_exist, action)
    print cmd
    proc =  subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, universal_newlines=True)
    #cmd_out = proc.stdout.read()

    if action == "status":
        none_found = True
        for cmd_out in iter(proc.stdout.readline, ""):
            if "not-found" in cmd_out or "unrecognized" in cmd_out:
                mod_utils.send_issue("service %s not found" %service, action_type, service)
                none_found = False
                break
            elif ("stopped" in cmd_out) or ("dead" in cmd_out):
                none_found = False
                if (error_msg == ''):
                    error_msg = "service {0} is down".format(service)

                mod_utils.error(error_msg, action_type, service)
                break
            elif "running" in cmd_out or "copyright" in cmd_out or "Copyright" in cmd_out:
                none_found = False
                mod_utils.health("ok",action_type, service)
                break
        proc.stdout.close()
        if none_found == True:
            mod_utils.send_issue("Could not get the status of service %s" %service, action_type, service)

if __name__ == "__main__":
    begin(args.action, args.service, args.service_path, args.error_msg)
