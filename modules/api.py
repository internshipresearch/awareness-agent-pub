__author__ = 'Gaurav'

'''
    Tool tell the API status
    Usage:
        --url : url to be checked e.g. facebook.com
    Methods
    wlog : logging function
    check_ping : method to test he ping
        return True if network is fine
        return False if there is some issue in the network
'''

import subprocess
import mod_utils
import argparse

action_type = "api"


parser = argparse.ArgumentParser()
parser.add_argument("--url", help="Api Url to be checked", default = "nope")
parser.add_argument("--method", default = "GET", help="request method for api")
parser.add_argument("--code", default = "200", help="response code of api")
parser.add_argument("--response", default = "", help="response of api")
parser.add_argument("--error_msg", help="Error msg which you want to send in alerts", default='')
args = parser.parse_args()

'''
1. We are checking the status code of api
2. if match then check for content
3.      if content matched raise health
4.      else raise issue - staus code is same but different content
5. raise error api not working
'''
def check_api(url, method, code, response, error_msg):
    global  action_type
    cmd_status_code = 'curl -X '+ method +' -s -o /dev/null -I -w ' + '"%{http_code}"'  + " -m 15 " + url
    mod_utils.wlog(cmd_status_code)
    proc = subprocess.Popen([cmd_status_code], stdout=subprocess.PIPE, shell=True)
    (status_code, err) = proc.communicate()

    if status_code == code:
        if response != "":
            cmd_api_content = "curl -X %s -m 15 %s" % (method, url)
            #print cmd_api_content
            proc = subprocess.Popen([cmd_api_content], stdout=subprocess.PIPE, shell=True)
            (api_content, err) = proc.communicate()
            #print api_content
            if response in api_content:
                mod_utils.wlog("api %s is working fine" %url)
                mod_utils.health('api is working fine', 'api', url)
            else:
                mod_utils.wlog("api %s response code is same but response content is different sending issue" %url)
                mod_utils.send_issue('api not responding as expected', 'api', url)
            return True
        else:
            mod_utils.wlog("api %s is working fine" %url)
            mod_utils.health('api is working fine', 'api', url)

    elif status_code == '000':
        mod_utils.wlog("Issue: api unreachable")
        mod_utils.send_issue("Issue: api unreachable", 'api', url)
    else:
        mod_utils.wlog("unexpected status_code: %s for api: %s" %(status_code, url))
        if (error_msg == ''):
            error_msg = "Error: unexpected status_code: {0} for api: {1}".format(status_code, url)
        mod_utils.error(error_msg, 'api', url)
        return False



if __name__ == "__main__":
    check_api(args.url, args.method, args.code, args.response, args.error_msg)
