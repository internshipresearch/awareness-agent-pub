__author__ = 'Gajender'
'''
    Tool tell the memcache status of the running server by testing memcache command result
    Usage:
        --host : host of the server
        --port : port of the memcache service
'''

import json
import mod_utils
import argparse

try:
    from pymemcache.client.base import Client
except Exception as e:
    mod_utils.wlog("pymemcache not found, please install it first to check memcache!")
    exit()

action_type = "memcache"

parser = argparse.ArgumentParser()
parser.add_argument("--host", help="public ip address", default = None)
parser.add_argument("--port", help="port number", type=int, default = None)
parser.add_argument("--error_msg", help="Error msg which you want to send in alerts", default='')
parser.add_argument("--key", help="memcache key", default = None)
args = parser.parse_args()


def json_serializer(key, value):
    """Converts to json string"""
    if type(value) == str:
        return value, 1
    return json.dumps(value), 2

def json_deserializer(key, value, flags):
    """Deserializes the json string"""
    if flags == 1:
        return value
    if flags == 2:
        return json.loads(value)
    raise Exception("Unknown serialization format")

def check_mem_key(host, port, error_msg, key):
    global  action_type
    try:
        mod_utils.wlog("connecting memcache host | %s"%host)
        client = Client(
            (host, port),
            serializer=json_serializer,
            deserializer=json_deserializer,connect_timeout=10)
        if client is None:
            mod_utils.wlog('Unable to connect with memcache at | %s'%host)
            mod_utils.send_issue('Unable to connect with memcache at | %s'%host, action_type, 'memcache')

        if key is not None:
            companies = client.get('companies')
            if companies is None:
                mod_utils.wlog('No data found in memcache | %s'%host)
                if (error_msg == ''):
                    error_msg = "No data found in memcache | {0}".format(host)
                mod_utils.error(error_msg, action_type, 'memcache')
                return False
            else:
                mod_utils.health('memcache ok', action_type, 'memcache')
                return True
        mod_utils.health('memcache ok', action_type, 'memcache')
        return True
    except Exception, e:
        mod_utils.error("Unable to connect with memcache at | %s | %s"%(host, e), action_type, 'memcache')
    return False

if __name__ == "__main__":
    if check_mem_key(args.host, args.port, args.error_msg, args.key):
        mod_utils.wlog("memcache looks good!!")
    else:
        mod_utils.wlog("Problem in memcache")
