__author__ = 'ashysaini'

'''
    CLI tool for checking if the file is getting updated or not
    if log file is not updating for n seconds then an alert will be generated
    it can also generate the alert if something is written in the file in last n seconds
    Arguments :
        --file : name of the file with full path
        --seconds (optional) : number of seconds for which the file is not written (default 60)
        --action (optional) : action to be performed by CLI (reverse : this option will check if the file is written
            to generate alert)
        --help : show help section
'''


import mod_utils
import os
import time
import argparse
import glob

action_type = "filechanged"

parser = argparse.ArgumentParser()
parser.add_argument('--file', default='none', help='Name of the file to be checked with full path, like /tmp/file.log')
parser.add_argument('--seconds', default=60, type=int, help='Number of seconds for which the file is not written')
parser.add_argument('--status', default=1, type=int, help='1 will check for file change and 0 will check for file not change in given seconds')

parser.add_argument('--action', default='status', help='For reversing the action, other option reverse')
parser.add_argument("--error_msg", help="Error msg which you want to send in alerts", default='')
args = parser.parse_args()

def begin(file_name, seconds, status, action, error_msg):
    global action_type
    file = file_name

    if '*' in file_name:
        '''
            * is the last character of our file name so
            spliting on * 0th index will give give us partial name of file with full path
            iglobe() will give us iterator of all matched files getctime() will give us last modified file so we will get
            required log file
        '''
        name = file_name.split('*')[0]
        try:
            file = max(glob.iglob(name+'*'), key=os.path.getctime)
        except Exception as e:
            mod_utils.send_issue("file with %s does not exist"%file_name,action_type, file_name)
            mod_utils.wlog('File %s does not exist'%file_name)
            return False
    else:
        if os.path.exists(file):
            'pass'
        else:
            mod_utils.send_issue("%s does not exist"%file,action_type, file_name)
            mod_utils.wlog('File %s does not exist'%file)
            return False

    #if file is not updating in n seconds then error
    last_time = os.path.getmtime(file)
    now = time.time()

    if status == 1:
        if now > (last_time + seconds):
            if (error_msg == ''):
                error_msg = "{0} not changed in last {1}sec".format(file, seconds)

            mod_utils.error(error_msg,action_type, file_name)
        else:
            mod_utils.health("ok",action_type, file_name)
        print (last_time)
    if status == 0:
        if now > (last_time + seconds):
            mod_utils.health("ok",action_type, file_name)
        else:
            if (error_msg == ''):
                error_msg = "{0} changed in last {1}sec".format(file, seconds)

            mod_utils.error(error_msg, action_type, file_name)



if __name__ == "__main__":
    begin(args.file, args.seconds, args.status, args.action, args.error_msg)
