__author__ = 'gaurav'

'''
    Tool tell the Command status
    Usage:
        --cmd : provide command which you want to check. eg. ps aux | grep python
        --nz_exit_code : checks error_text in case of non-zero exit code
        --success_text : text which you want in this command's output for success
        --error_text: text which you want in this command's output to send u error alert
    Methods
    check_cmd : method to test the command
        runs the given command and checks output for sending error, issue and health.
'''

import subprocess
import mod_utils
import argparse

action_type = "cmd"

parser = argparse.ArgumentParser()
parser.add_argument("--cmd", help="Command to be exicuted", default = "nope")
parser.add_argument("--nz_exit_code", help="If you want to check error_text for non zero exit code", default = "1")
parser.add_argument("--success_text", help="Success text which you want to match", default = "88888888abcd")
parser.add_argument("--error_text", help="Error text which you want to match", default = "88888888abcd")
parser.add_argument("--error_msg", help="Error msg which you want to send in alerts", default='')
parser.add_argument("--name", help="Name of your command", default='')
args = parser.parse_args()

def check_cmd(cmd, nz_exit_code, success_text, error_text, error_msg, name):
    global  action_type
    cmd = cmd.strip()
    # originnal cmd to send to es in message
    config_command = cmd

    # appending command to check status code
    if cmd[-1] == ';':
        cmd = cmd + " echo $?"
    else:
        cmd = cmd + "; echo $?"

    proc =  subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
    stdout, err = proc.communicate()
    try:
        exit_code = stdout.split('\n')[-2].strip()
        output = ''.join(stdout.split('\n')[:-2]).strip()
    except Exception as e:
        exit_code = 1
        output = None

    mod_utils.wlog('cmd output:\n%s'%output)

    if (error_msg == ''):
        error_msg = "cmd: {0} : output : {1}".format(config_command, output)
    else:
        error_msg += ": output : {0}".format(output)

    if (name == ''):
        name = config_command

    '''
    if success_text is set and error text is not set
        if success_text in output
            OK
        else:
            error
    if error_text is set and success text is not set
        if error_text in output
            error
        else
            OK
    if success_text is set and error_text is set
        if success_text in output
            OK
        if error_text in output
            error
        else
            issue
    '''

    if exit_code != "0":
        mod_utils.send_issue("cmd: %s : giving unexpected output"%config_command,action_type, name)
        return

    if success_text != "88888888abcd" and error_text == "88888888abcd":
        if (success_text in output and success_text != '') or (success_text == '' and success_text == output):
            # if success text matches then send health
            mod_utils.health("ok",action_type, name)
        else:
            mod_utils.error(error_msg,action_type, name)
        return
    if success_text == "88888888abcd" and error_text != "88888888abcd":
        if (error_text in output and error_text != '') or (error_text == '' and error_text == output):
            mod_utils.error(error_msg,action_type, name)
        else:
            mod_utils.health("ok",action_type, name)
        return
    if success_text != "88888888abcd" and error_text != "88888888abcd":
        if (success_text in output and success_text != '') or (success_text == '' and success_text == output):
            # if success text matches then send health
            mod_utils.health("ok",action_type, name)
        elif (error_text in output and error_text != '') or (error_text == '' and error_text == output):
            mod_utils.error(error_msg,action_type, name)
        else:
            mod_utils.send_issue("cmd: %s : giving unexpected output"%config_command,action_type, name)
        return
    else:
        mod_utils.send_issue("cmd: %s : Both error and success text are not set, module require atleast 1"%config_command,action_type, name)

if __name__ == "__main__":
    check_cmd(args.cmd, args.nz_exit_code, args.success_text, args.error_text, args.error_msg, args.name)
