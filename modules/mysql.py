__author__ = 'ashysaini'

'''
    CLI tool for checking the database connection to the database server
    this module will check the db connection based on the parameters provided
    if not able to connect the database, alert will be generated (can be used vise-versa)
    Arguments :
        --host : hostname of database connection
        --username : username of database
        --password : password of database
        --action (optional) : action to be performed by CLI
        --help : show help section
    package required : mysql_client to check the connection to db
'''

import mod_utils
import subprocess
import argparse
import time

action_type = "mysql"

#click package is included to parse command line arguments
"""@click.command()
@click.option('--hostname', default='localhost', help='Hostname of mysql database')
@click.option('--name', default='localhost', help='Name of the action')
@click.option('--username', default='root', help='Username of mysql database')
@click.option('--password', default='', help='Password of mysql database')
@click.option('--action', default='status', help='Functionality not defined yet, for future use')
"""
parser = argparse.ArgumentParser()
parser.add_argument("--hostname", default = "localhost", help="Hostname of mysql database")
parser.add_argument("--name", default = "local connection", help="Name of the action")
parser.add_argument("--username", default = "root", help="Username of mysql database")
parser.add_argument("--password", default = " ", help="Password of mysql database")
parser.add_argument("--action", default = "status", help="Functionality not defined yet, for future use")
parser.add_argument("--error_msg", help="Error msg which you want to send in alerts", default='')
args = parser.parse_args()

def begin(name, hostname, username, password, action, error_msg):
    global action_type
    cmd = "timeout 5s mysql -h'%s' -u'%s' -p'%s' -e'show databases;'" %(hostname, username, password)
    print cmd
    #exit()
    proc =  subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
    cmd_out = proc.stdout.read()
    print cmd_out
    mod_utils.wlog(cmd_out)

    if "information_schema" in cmd_out:
        mod_utils.health("ok", action_type, name)
    else:
        #mod_utils.send_issue("mysql down error",action_type, name)
        if (error_msg == ''):
            error_msg = "Error: mysql down"
        mod_utils.error(error_msg, action_type, name)
    # time.sleep(5)

if __name__ == "__main__":
    begin(args.name, args.hostname, args.username, args.password, args.action, args.error_msg)




