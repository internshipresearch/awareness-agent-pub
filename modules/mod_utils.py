__author__ = 'ashysaini'

import json
import time
import uuid
import os
import urllib
import urllib2
import logging
from logging.handlers import TimedRotatingFileHandler

LOG_PATH = '/log'
PATH = os.path.dirname(os.path.abspath(__file__))
CONFIG_FILE = PATH + '/../config/config.json'


#read the data from config file
with open(CONFIG_FILE, 'r') as config_file:
    config_data = config_file.read()
    config_file.close()

    config = json.loads(config_data)

    #check if log directory exists if doesn't create it
    # if not os.path.exists(LOG_PATH):
    #     try:
    #         os.makedirs(LOG_PATH)
    #     except OSError:
    #         if not os.path.isdir(LOG_PATH):
    #             print "Log folder does not exist, try creating it. %s" %LOG_PATH
    #             exit()


def create_logger():
    l = logging.getLogger('agent')
    formatter = logging.Formatter('%(asctime)s : %(message)s')
    fileHandler = logging.FileHandler('/tmp/agent.log', mode='a+')
    fileHandler.setFormatter(formatter)
    streamHandler = logging.StreamHandler()
    streamHandler.setFormatter(formatter)
    l.setLevel(logging.INFO)
    l.addHandler(fileHandler)
    l.addHandler(streamHandler) 
    return logging.getLogger('agent')

#creating logger object
agent_logger = create_logger()


#check supplied arguments args vs minimum arguments required
def check_arguments(args, req):
    if len(args) < req:
        error("Arguments supplied are none Or Less than required.")
        exit()

#method to send issue to central server
def send_issue(msg, type="service", name="none"):
    server = config['server_id']
    cur = int(time.time())
    issue = { "id" : uuid.uuid1().int>>80, "time" : cur, "server" : server, "action" : "issue", "type" : type, "name": name , "msg" : msg}
    print(issue)
    try:
        issue = json.dumps(issue)
        api_action(issue)
    except Exception as e:
        print ("Problem in making json!")



#error encountred while checking the status
def error(msg, type="service", name="none"):
    print ("Error : %s" %msg)
    server = config['server_id']
    cur = int(time.time())
    error = {"id" : uuid.uuid1().int>>80,  "time" : cur, "server" : server, "action" : "error", "type" : type, "name": name , "msg" : msg}
    print(error)
    try:
        error = json.dumps(error)
        api_action(error)
    except Exception as e:
        print ("Problem in making json!")


#send health status to central server
def health(msg, type="service", name="none"):
    server = config['server_id']
    cur = int(time.time())
    health = {"id" : uuid.uuid1().int>>80, "time" : cur, "server" : server, "action" : "health", "type" : type,"name": name, "msg":msg}
    print(health)
    API_DAT = PATH + '/../logs/api.dat'
    target = open(API_DAT, 'a')
    target.write(json.dumps(health))
    target.write("\n")
    target.close()

def wlog(msg):
    agent_logger.info(msg)


def api_action(line):
    es_url = config['es_url']
    auth = config['authorization']
        
    headers = {"Authorization": auth}
    data = json.loads(line)
    data = urllib.urlencode(data)
    req = urllib2.Request(es_url, data, headers)
    try:
        response = urllib2.urlopen(req)
    except Exception as e:
        print "Unable to connect"
        print e.reason
        exit()
    the_page = response.read()
    print response.getcode()
    print the_page
    response.close()
