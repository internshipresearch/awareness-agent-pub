__author__ = 'ashysaini'

'''
    CLI tool for checking if the script is running or not
    this module will check the running based on the parameters provided
    if the script is not running then an alert will be generated (can be used vise-versa)
    Arguments :
        --script : name of the script to check if this is running
        --action (optional) : action can be "count", will return the count of running processes of script
        --help : show help section
'''

import mod_utils
import subprocess
import time
import argparse

action_type = "scriptrunning"

parser = argparse.ArgumentParser()
parser.add_argument('--script', default='none', help='Name of the service, like apache2 mysql etc.')
parser.add_argument('--action', default='status', help='optional : if action is "count", return count of running sctipts')
args = parser.parse_args()


def begin(action, script):
    global  action_type

    cmd = "ps aux | grep '%s' | wc -l | tr -d [:space:] " %(script)

    #return if the script is running
    proc =  subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
    cmd_out = proc.stdout.read()

    actual_count = int(cmd_out) - 4

    if action == "count":
        if actual_count < 1:
            mod_utils.send_issue("script not running",action_type, script)
        else:
            mod_utils.health("", action_type, script)
    else:
        if actual_count < 1:
            mod_utils.send_issue("script not running",action_type, script)
        else:
            mod_utils.health("",action_type, script)


    mod_utils.wlog(cmd_out)
    time.sleep(5)


if __name__ == "__main__":
    begin(args.action, args.script)

