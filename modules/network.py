__author__ = 'ashysaini'
'''
    Tool tell the network status of the running server by testing ping result
    Usage:
        --url : url to be checked e.g. facebook.com
    Methods
    wlog : logging function
    check_ping : method to test he ping
        return True if network is fine
        return False if there is some issue in the network
'''

import subprocess
import mod_utils
import argparse
import time

action_type = "network"

parser = argparse.ArgumentParser()
parser.add_argument("--url", help="Url to be checked", default = "nope")
parser.add_argument("--error_msg", help="Error msg which you want to send in alerts", default='')
args = parser.parse_args()

def check_ping(url, error_msg):
    global  action_type
    mod_utils.wlog("ping -c 3 %s | grep loss" %url)
    #proc = subprocess.Popen(["timeout 5s ping -c 3 %s | grep loss" %url], stdout=subprocess.PIPE, shell=True)
    proc = subprocess.Popen(["ping -c 3 %s | grep loss" %url], stdout=subprocess.PIPE, shell=True)
    (out, err) = proc.communicate()
    mod_utils.wlog("program output:"+out)

    if "packet loss" in out:
        loss = out.split(",")[2]
        percent = loss.split("%")[0].strip(" ")
        mod_utils.wlog("Packet loss percent = %s" %percent)
        percentInt = int(float(percent))

        if percentInt > 20:
            mod_utils.wlog("High packet loss!! %d" %percentInt)
            mod_utils.send_issue("High packet loss in %s" %url ,action_type, url)
            return False
        else:
            mod_utils.health("ok", action_type, url)
            mod_utils.wlog("Ping looks OK!!")
            return True
    else:
        print "Connection Unreachable!!"
        if (error_msg == ''):
            error_msg = "Connection Unreachable to {0}".format(url)

        mod_utils.error(error_msg, action_type, url)
        return False

if __name__ == "__main__":
    if check_ping(args.url, args.error_msg):
        print "Network looks good!!"
    else:
        print "Problem in network"
